import { addFavorites } from "../addFavorites/addFavorites";
import { updateFavoriteCards } from "../updateFavoriteCards/updateFavoriteCards";

function createMovieCard(movie: IMovie): HTMLDivElement {
    const { poster_path, title, overview, release_date, id } = movie;
    let favorites: Array<number> = [];
    const localFavorites = localStorage.getItem('favorites');
    if (localFavorites) favorites = JSON.parse(localFavorites)
    const favorite: boolean = favorites.includes(id);

    const handleAddFavorites = (): void => {
        const favorite: boolean = addFavorites(id);
        updateFavoriteCards();
        svg?.setAttribute('fill', favorite ? "red" : "#ff000078");
    };

    const card = document.createElement('div');
    card.className = 'col-lg-3 col-md-4 col-12 p-2';
    card.innerHTML = `
        <div class="card shadow-sm">
            <img src="https://image.tmdb.org/t/p/original/${poster_path}" alt="${title}" />
            <svg xmlns="http://www.w3.org/2000/svg" stroke="red" fill=${favorite ? "red" : "#ff000078"} width="50" height="50"
                class="bi bi-heart-fill position-absolute p-2" viewBox="0 -2 18 22" >
                <path fill-rule="evenodd" d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z" />
            </svg>
            <div class="card-body">
                <p class="card-text truncate">
                    ${overview}
                </p>
                <div class="
                            d-flex
                            justify-content-between
                            align-items-center
                            ">
                    <small class="text-muted">${release_date}</small>
                </div>
            </div>
        </div>
    `;

    const svg = card.querySelector('svg');
    svg?.addEventListener('click', handleAddFavorites)
    return card;
}

export { createMovieCard };