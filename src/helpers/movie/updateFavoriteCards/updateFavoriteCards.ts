import { createMovieCard } from '../createMovieCard/createMovieCard';
import { movieAPI } from '../../../services/services';

function updateFavoriteCards(): void {
    const favoriteContainer = document.getElementById('favorite-movies');
    let favorites: Array<number> = [];
    const localFavorites = localStorage.getItem('favorites');
    if (localFavorites) favorites = JSON.parse(localFavorites);

    if (favoriteContainer?.children.length) {
        while (favoriteContainer?.firstChild) favoriteContainer.firstChild.remove();
    }

    favorites.forEach(async (item) => {
        const movie: any = await movieAPI.getMovieById(item);
        const card = createMovieCard(movie);
        card.className = 'col-12 p-2';


        favoriteContainer?.appendChild(card);
    })
}

export { updateFavoriteCards };