function addFavorites(id: number): boolean {
    let favorites: Array<number> = [];
    const localFavorites = localStorage.getItem('favorites');

    if (localFavorites) {
        favorites = JSON.parse(localFavorites);
        const favoriteId: number = favorites.indexOf(id);
        favoriteId !== -1 ? favorites.splice(favoriteId, 1) : favorites.push(id);
    } else {
        favorites.push(id);
    }

    localStorage.setItem('favorites', JSON.stringify(favorites));

    const favorite: boolean = favorites.includes(id);
    return favorite;
}

export { addFavorites };