import { API_URL, API_KEY } from "../../common/enums/enums";

export async function helperMovieAPI(endpoint: string, method: string, urlParams = {}, data = {}): Promise<void> {
    let addParams = '';
    if (Object.keys(urlParams).length) {
        const keys = Object.keys(urlParams);
        const values = Object.values(urlParams);

        addParams = keys.map((item, index) => {
            return `&${keys[index]}=${values[index]}`;
        }).join('');
    }
    const options = {
        method,
        data
    }

    const response = await fetch(API_URL + endpoint + '?api_key=' + API_KEY + addParams, options);
    const result = await response.json();

    return result;
}

