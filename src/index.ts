import { movieAPI } from './services/services';
import {
    createMovieCard,
    movieMapper,
    createRandomMovieCard,
    updateFavoriteCards
} from "./helpers/helpers";

export async function render(): Promise<void> {
    // TODO render your app here
    const searchInput = document.getElementById('search') as HTMLInputElement;
    const loadMoreBtn = document.getElementById('load-more') as HTMLButtonElement;
    const movieContainer = document.getElementById('film-container') as HTMLElement;
    const randomMovieContainer = document.getElementById('random-movie') as HTMLElement;
    const radioBtns = document.querySelectorAll('input[name="btnradio"]') as NodeList;
    const favoriteBtn = document.getElementById('favorites-btn') as HTMLButtonElement;

    let page = 1;
    let query = '';
    let category = 'popular';

    async function updatesCards(page: number, category: string, query: string): Promise<void> {
        let data: any;

        switch (category) {
            case 'popular': data = await movieAPI.getPopular({ page });
                break;
            case 'upcoming': data = await movieAPI.getUpcoming({ page });
                break;
            case 'top_rated': data = await movieAPI.getTopRated({ page });
                break;
            case 'search': data = await movieAPI.getSearch({ page, query });
                break;
            default: data = await movieAPI.getPopular({ page });
        }

        const movies = movieMapper(data);
        const randomCard = createRandomMovieCard(movies, randomMovieContainer as HTMLElement);

        if (randomMovieContainer?.children.length && movies.length) {
            randomMovieContainer.removeChild(randomMovieContainer?.children[0]);
        }

        if (randomCard) randomMovieContainer?.appendChild(randomCard);

        movies.forEach(movie => {
            const card = createMovieCard(movie);
            movieContainer?.appendChild(card);
        })
    }

    function handleSearch(e: Event): void {
        const target = e.currentTarget as HTMLInputElement;
        page = 1;
        category = target.id;
        query = target.value;
        while (movieContainer?.firstChild) movieContainer.firstChild.remove();
        updatesCards(page, category, query);
        target.value = '';
    }

    function handleSwitchCategory(e: Event): void {
        const { id } = e.currentTarget as HTMLInputElement;
        category = id;
        page = 1;
        while (movieContainer?.firstChild) movieContainer.firstChild.remove();
        updatesCards(page, category, query);
    }

    function handleLoadMore(): void {
        page = page + 1
        updatesCards(page, category, query);
    }

    searchInput?.addEventListener('change', (e) => handleSearch(e));
    radioBtns?.forEach(btn => btn.addEventListener('change', (e) => handleSwitchCategory(e)));
    loadMoreBtn?.addEventListener('click', () => handleLoadMore());
    favoriteBtn?.addEventListener('click', () => updateFavoriteCards());

    movieContainer.innerHTML = query;

    await updatesCards(page, category, query);
    updateFavoriteCards();
}
