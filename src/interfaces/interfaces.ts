interface IurlParams {
    page?: number,
    language?: string,
    query?: string,
    id?: number
}

interface IMovie {
    poster_path: string,
    backdrop_path: string,
    title: string,
    overview: string,
    release_date: string,
    id: number
}