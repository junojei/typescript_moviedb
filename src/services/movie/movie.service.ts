import { ROUTES } from '../../common/enums/enums';
import { helperMovieAPI } from '../../helpers/helpers';

class MovieApi {

    async getPopular(urlParams: IurlParams): Promise<void> {
        return await helperMovieAPI(ROUTES.POPULAR, 'GET', urlParams);
    }

    async getTopRated(urlParams: IurlParams): Promise<void> {
        return await helperMovieAPI(ROUTES.TOP_RATED, 'GET', urlParams);
    }

    async getUpcoming(urlParams: IurlParams): Promise<void> {
        return await helperMovieAPI(ROUTES.UPCOMING, 'GET', urlParams)
    }

    async getSearch(urlParams: IurlParams): Promise<void> {
        return await helperMovieAPI(ROUTES.SEARCH, 'GET', urlParams)
    }

    async getMovieById(id: number): Promise<void> {
        return await helperMovieAPI(ROUTES.GET_MOVIE + id, 'GET');
    }
}

export { MovieApi };